using Microsoft.AspNetCore.Mvc;
using NotificationSender.Models;
using NotificationSender.Models.Api;
using AutoMapper;
using NotificationSender.Models.Senders;
using NotificationSender.Models.Senders.Push;
using NotificationSender.Models.Storage;
using NotificationSender.Services.Senders;
using NotificationSender.Services.Storage;

namespace NotificationSender.Controllers;

[ApiController]
[Route("[controller]")]
public class NotificationsController : ControllerBase
{

    private readonly ILogger<NotificationsController> _logger;
    private readonly IMessageStorage _messageStorage;
    private readonly IMapper _mapper;

    /// <summary>
    /// Conroller for notification management
    /// </summary>
    public NotificationsController(ILogger<NotificationsController> logger, IMessageStorage messageStorage)
    {
        _mapper = new MapperConfiguration(cfg => 
        {
            cfg.CreateMap<CreateIOSPushRequest, IOSMessage>();
            cfg.CreateMap<CreateAndroidPushRequest, AndroidMessage>();
            cfg.CreateMap<StoreMessage, NotificationResponse>()
                .ForMember(dst => dst.State, dst => dst.MapFrom(src => src.State.GetString()));
        }).CreateMapper();
        _logger = logger;
        _messageStorage = messageStorage;
    }

    /// <summary>
    /// Send push message for iOS platform
    /// </summary>
    /// <param name="request">iOS push message body</param>
    /// <returns>Returns id of created notification and it's sending state</returns>
    [HttpPost("push/ios")]
    [Consumes("application/json")]
    public async Task<ActionResult<NotificationResponse>> CreateIOSPush(
        [FromBody] CreateIOSPushRequest request, 
        [FromServices] INotificationSender<IOSMessage> notificationSender)
    {
        try
        {
            var messageToSend = _mapper.Map<IOSMessage>(request);
            var result = await ProcessMessage<IOSMessage>(messageToSend, notificationSender);
            return result;
        }
        catch (Exception ex)
        {
            _logger.LogError($"[{ex.GetType()}]: {ex.Message}");
            return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }
        
    }

    /// <summary>
    /// Send push message for Android platform
    /// </summary>
    /// <param name="request">Android push message body</param>
    /// <returns>Returns id of created notification and it's sending state</returns>
    [HttpPost("push/android")]
    [Consumes("application/json")]
    public async Task<ActionResult<NotificationResponse>> CreateAndroidPush(
        [FromBody] CreateAndroidPushRequest request,
        [FromServices] INotificationSender<AndroidMessage> notificationSender)
    {
        try
        {
            var messageToSend = _mapper.Map<AndroidMessage>(request);
            var result = await ProcessMessage<AndroidMessage>(messageToSend, notificationSender);
            return result;
        }
        catch (Exception ex)
        {
            _logger.LogError($"[{ex.GetType()}]: {ex.Message}");
            return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Get notification state
    /// </summary>
    /// <param name="id">Notification identifier</param>
    /// <returns>Returns sending state for notification message</returns>
    [HttpGet("{id}/status")]
    public async Task<ActionResult<string>> GetNotificationStatus([FromRoute] string id)
    {
        try
        {
            var message = await _messageStorage.GetMessageAsync(id);
            if (message is null)
            {
                return NotFound($"Notification with id {id} doesn't exists");
            }
            return message.State.GetString();
        }
        catch (Exception ex)
        {
            _logger.LogError($"[{ex.GetType()}]: {ex.Message}");
            return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Send message of type IMessage and save it's sending state to storage
    /// </summary>
    /// <param name="message">Message to send</param>
    /// <param name="sender">Notification sender that will be used to send message</param>
    /// <returns>Returns id of created notification and it's sending state</returns>
    private async Task<NotificationResponse> ProcessMessage<T>(T message, INotificationSender<T> sender) where T:IMessage
    {
        var storedMessage = await _messageStorage.CreateMessageAsync();
        var sendSuccessful = await sender.SendMessageAsync(message);

        storedMessage.State = sendSuccessful ? SendMessageStates.Successful : SendMessageStates.Failed;
        await _messageStorage.UpdateMessageAsync(storedMessage.Id, storedMessage.State);

        var response = _mapper.Map<NotificationResponse>(storedMessage);
        return response;
    }
}
