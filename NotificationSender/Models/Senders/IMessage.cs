namespace NotificationSender.Models.Senders
{
    /// <value>
    /// An interface for message that should be sent
    /// </value>
    public interface IMessage
    {
    }
}