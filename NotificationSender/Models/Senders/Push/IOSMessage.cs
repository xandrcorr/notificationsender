namespace NotificationSender.Models.Senders.Push
{
    /// <summary>
    /// Represents push message for iOS platform
    /// </summary>
    public sealed class IOSMessage : PushMessage
    {
        /// <value>
        /// Token of push receiver device
        /// </value>
        public string PushToken { get; set; }

        /// <value>
        /// Push notification message text
        /// </value>
        public string Alert { get; set; }

        /// <value>
        /// Priority of something
        /// </value>
        public int Priority { get; set; }

        /// <value>
        /// Represents that something should be run in the background
        /// </value>
        public bool IsBackground { get; set; }

        public override string ToString()
        {
            return Alert;
        }
    }
}