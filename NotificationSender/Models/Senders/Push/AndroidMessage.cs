namespace NotificationSender.Models.Senders.Push
{
    /// <summary>
    /// Represents push message for Android platform
    /// </summary>
    public sealed class AndroidMessage : PushMessage
    {
        /// <value>
        /// Token of push receiver device
        /// </value>
        public string DeviceToken { get; set; }
        /// <value>
        /// Push notification message text
        /// </value>
        public string Message { get; set; }
        /// <value>
        /// Push notification message title
        /// </value>
        public string Title { get; set; }
        /// <value>
        /// Some condition
        /// </value>
        public string Condition { get; set; }

        public override string ToString()
        {
            return $"{Title}: {Message}";
        }
    }
}