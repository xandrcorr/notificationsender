namespace NotificationSender.Models.Senders.Push
{
    /// <summary>
    /// Abstract class for representing push messages
    /// </summary>
    public abstract class PushMessage : IMessage
    {
    }
}