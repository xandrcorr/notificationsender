namespace NotificationSender.Models
{
    /// <summary>
    /// Notification sending states
    /// </summary>
    public enum SendMessageStates
    {
        Created,
        Successful,
        Failed
    }

    /// <summary>
    /// Extension to convert notification sending states enum to string and vise versa
    /// </summary>
    public static class SendMessageStatesExtension
    {
        /// <summary>
        /// Extension method for converting current SendMessageStates value to string representation
        /// </summary>
        /// <returns>String representation of current SendMessageStates value</returns>
        public static string GetString(this SendMessageStates state)
        {
            switch (state)
            {
                case SendMessageStates.Created: return "created";
                case SendMessageStates.Successful: return "successful";
                case SendMessageStates.Failed: return "failed";
                default: throw new NotImplementedException($"Message state {(int)state} doesn't exists");
            }
        }

        /// <summary>
        /// Extension method for getting SendMessageStates value from string
        /// </summary>
        /// <returns>SendMessageState value</returns>
        public static SendMessageStates ToSendMessageState(this string stateString)
        {
            switch (stateString)
            {
                case "created": return SendMessageStates.Created;
                case "successful": return SendMessageStates.Successful;
                case "failed": return SendMessageStates.Failed;
                default: throw new NotImplementedException($"Message state {stateString} doesn't exists");
            }
        }
    }
}
