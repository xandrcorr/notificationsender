namespace NotificationSender.Models.Storage
{
    /// <summary>
    /// Represents stored notification message
    /// </summary>
    public class StoreMessage
    {
        /// <value>
        /// Notification message identifier (string-represented Guid)
        /// </value>
        public string Id { get; set; }
        /// <value>
        /// Enumerable message state
        /// </value>
        public SendMessageStates State { get; set; }
    }
}
