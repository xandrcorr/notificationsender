using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NotificationSender.Models.Api
{
    /// <summary>
    /// iOS push message request body
    /// </summary>
    public class CreateIOSPushRequest
    {
        /// <value>
        /// Token of push receiver device
        /// </value>
        [Required]
        [StringLength(50)]
        public string PushToken { get; set; }
        
        /// <value>
        /// Push notification message text
        /// </value>
        [Required]
        [StringLength(2000)]
        public string Alert { get; set; }

        /// <value>
        /// Priority of something
        /// </value>
        [DefaultValue(10)]
        public int Priority { get; set; } = 10;

        /// <value>
        /// Represents that something should be run in the background
        /// </value>
        [DefaultValue(true)]
        public bool IsBackground { get; set; } = true;
    }
}