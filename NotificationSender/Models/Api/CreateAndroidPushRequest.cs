using System.ComponentModel.DataAnnotations;

namespace NotificationSender.Models.Api
{
    /// <summary>
    /// Android push message request body
    /// </summary>
    public class CreateAndroidPushRequest
    {
        /// <value>
        /// Token of push receiver device
        /// </value>
        [Required]
        [StringLength(50)]
        public string DeviceToken { get; set; }

        /// <value>
        /// Push notification message text
        /// </value>
        [Required]
        [StringLength(2000)]
        public string Message { get; set; }
        
        /// <value>
        /// Push notification message title
        /// </value>
        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        /// <value>
        /// Some condition
        /// </value>
        [StringLength(2000)]
        public string Condition { get; set; }
    }
}