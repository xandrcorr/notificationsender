namespace NotificationSender.Models.Api
{
    /// <summary>
    /// Represents notification message in api
    /// </summary>
    public class NotificationResponse
    {
        /// <value>
        /// Notification message identifier (string-represented Guid)
        /// </value>
        public string Id { get; set; }
        /// <value>
        /// Message sending state (created, successful, failed)
        /// </value>
        public string State { get; set; }
    }
}