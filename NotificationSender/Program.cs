using NotificationSender.Models.Senders.Push;
using NotificationSender.Services;
using NotificationSender.Services.Senders;
using NotificationSender.Services.Senders.Push;
using NotificationSender.Services.Storage;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<INotificationSender<AndroidMessage>,AndroidPushSender>();
builder.Services.AddSingleton<INotificationSender<IOSMessage>,IOSPushSender>();
builder.Services.AddTransient<IDelayProvider, RandomDelayProvider>();
builder.Services.AddSingleton<IMessageStorage, RedisMessageStorage>();

builder.Services.Configure<RedisStorageOptions>(builder.Configuration.GetSection("Storage"));

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
