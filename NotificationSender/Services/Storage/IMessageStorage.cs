using NotificationSender.Models;
using NotificationSender.Models.Storage;

namespace NotificationSender.Services.Storage
{
    /// <summary>
    /// Interface for storing notification message states
    /// </summary>
    public interface IMessageStorage
    {
        /// <summary>
        /// Create new notification
        /// </summary>
        /// <returns>StoreMessage object with newly generated Id (string-represented Guid) and Created state</returns>
        Task<StoreMessage> CreateMessageAsync();
        
        /// <summary>
        /// Get notification with specific identifier
        /// </summary>
        /// <param name="messageId">Message identifier (string-represented Guid)</param>
        /// <returns>StoreMessage that corresponts to passed identifier</returns>
        Task<StoreMessage> GetMessageAsync(string messageId);

        /// <summary>
        /// Update state of specific message
        /// </summary>
        /// <param name="messageId">Message identifier (string-represented Guid)</param>
        /// <param name="messageState">Message identifier (string-represented Guid)</param>
        /// <returns>Updated StoreMessage</returns>
        Task<StoreMessage> UpdateMessageAsync(string messageId, SendMessageStates messageState);
    }
}
