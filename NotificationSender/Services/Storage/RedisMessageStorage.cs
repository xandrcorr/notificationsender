using StackExchange.Redis;
using NotificationSender.Models.Storage;
using NotificationSender.Models;
using Microsoft.Extensions.Options;

namespace NotificationSender.Services.Storage
{
    /// <summary>
    /// Implementation of IMessageStorage that uses Redis to store notification message states
    /// </summary>
    /// <remarks>
    /// Note that message state stored in Redis as string representation (see <see cref="NotificationSender.Models.SendMessageStatesExtension"/> class for details)
    /// </remarks>
    public class RedisMessageStorage : IMessageStorage
    {
        private readonly ConnectionMultiplexer client;

        public RedisMessageStorage(IOptions<RedisStorageOptions> options)
        {
            client = ConnectionMultiplexer.Connect(options.Value.ConnectionString);
        }

        public async Task<StoreMessage> CreateMessageAsync()
        {
            var message = new StoreMessage()
            {
                Id = Guid.NewGuid().ToString(),
                State = SendMessageStates.Created
            };

            var db = client.GetDatabase();
            await db.StringSetAsync(message.Id, message.State.GetString());
            return message;
        }

        public async Task<StoreMessage> GetMessageAsync(string messageId)
        {
            var db = client.GetDatabase();
            var messageState = await db.StringGetAsync(messageId);
            if (messageState.IsNullOrEmpty)
            {
                return null;
            }

            return new StoreMessage()
            {
                Id = messageId,
                State = messageState.ToString().ToSendMessageState()
            };
        }

        public async Task<StoreMessage> UpdateMessageAsync(string messageId, SendMessageStates messageState)
        {
            var db = client.GetDatabase();
            var message = await db.StringSetAsync(messageId, messageState.GetString());
            return new StoreMessage()
            {
                Id = messageId,
                State = messageState
            };
        }
    }
}
