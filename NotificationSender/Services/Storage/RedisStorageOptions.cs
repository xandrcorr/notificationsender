namespace NotificationSender.Services.Storage
{
    /// <summary>
    /// Class for passing Redis storage configuration
    /// </summary>
    public class RedisStorageOptions
    {
        /// <value>
        /// Redis connection string (string with format "host:port", e.g. "localhost:6379")
        /// </value>
        public string ConnectionString { get; set; }
    }
}
