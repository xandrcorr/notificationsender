namespace NotificationSender.Services
{
    /// <summary>
    /// A class that provides random delay in range from 500ms to 2s
    /// </summary>
    public class RandomDelayProvider : IDelayProvider
    {
        /// <summary>
        /// Provides random delay value in range from 500ms to 2s
        /// </summary>
        /// <returns>Delay value in milliseconds</returns>
        public int GetDelay()
        {
            var rand = new Random();
            return rand.Next(500, 2000);
        }
    }
}