namespace NotificationSender.Services
{
    /// <summary>
    /// An interface for providing delay value
    /// </summary>
    public interface IDelayProvider
    {
        /// <summary>
        /// Provides specific delay value
        /// </summary>
        /// <returns>Delay value in milliseconds</returns>
        int GetDelay();
    }
}