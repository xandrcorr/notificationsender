using NotificationSender.Models.Senders.Push;

namespace NotificationSender.Services.Senders.Push
{
    /// <summary>
    /// Class that implements logic for sending push messages for iOS platform
    /// </summary>
    public sealed class IOSPushSender : BasePushSender<IOSMessage>
    {
        public IOSPushSender(IDelayProvider delayProvider, ILoggerFactory loggerFactory = null) : base(delayProvider, loggerFactory)
        {
        }
    }
}