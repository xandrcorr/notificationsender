using NotificationSender.Models.Senders.Push;

namespace NotificationSender.Services.Senders.Push
{
    /// <summary>
    /// Class that implements logic for sending push messages for Android platform
    /// </summary>
    public sealed class AndroidPushSender : BasePushSender<AndroidMessage>
    {
        public AndroidPushSender(IDelayProvider delayProvider, ILoggerFactory loggerFactory = null) : base(delayProvider, loggerFactory)
        {
        }
    }
}