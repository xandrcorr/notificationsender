using NotificationSender.Models.Senders.Push;

namespace NotificationSender.Services.Senders.Push
{
    /// <summary>
    /// Class that implements base logic for sending push messages
    /// </summary>
    public abstract class BasePushSender<T> : INotificationSender<T> where T: PushMessage
    {
        private int sentCount = 1;
        private object locker = new object();
        private readonly ILogger _logger;
        private readonly IDelayProvider _delayProvider;
        private readonly bool _loggingEnabled;

        protected BasePushSender(IDelayProvider delayProvider, ILoggerFactory loggerFactory)
        {
            var className = this.GetType().Name;
            _delayProvider = delayProvider;
            // turn off logging for tests
            if (loggerFactory is not null)
            {
                _logger = loggerFactory.CreateLogger(className);
                _logger.LogInformation($"{className} is initialized");
                _loggingEnabled = true;
            }
            else
            {
                _loggingEnabled = false;
            }
        }

        /// <summary>
        /// Send provided push message
        /// </summary>
        /// <returns>Returns true if send is successful, in other way returns false</returns>
        public async Task<bool> SendMessageAsync(T message)
        {
            var delay = _delayProvider.GetDelay();
            await Task.Delay(delay);
            bool sendingResult = true;
            lock (locker)
            {
                if (sentCount % 5 == 0)
                {
                    sentCount = 0;
                    sendingResult = false;
                }
                sentCount++;
            }
            
            var sendingResultStr = sendingResult ? "sent" : "not sent";

            if (_loggingEnabled)
            {
                _logger.LogInformation($"Message \"{message.ToString()}\" {sendingResultStr} with delay {delay}ms");
            }
            
            return sendingResult;
        }
    }
}