using NotificationSender.Models.Senders;

namespace NotificationSender.Services.Senders
{
    /// <summary>
    /// A generic interface for message sending
    /// </summary>
    public interface INotificationSender<T>  where T : IMessage
    {
        /// <summary>
        /// Send message of specific type
        /// </summary>
        /// <returns>Returns true if message send succeffsully, in other way returns false</returns>
        Task<bool> SendMessageAsync(T message);
    }
}