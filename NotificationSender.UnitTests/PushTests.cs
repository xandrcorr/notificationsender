using System;
using Moq;
using NUnit.Framework;
using NotificationSender.Models;
using NotificationSender.Models.Senders.Push;
using NotificationSender.Services;
using NotificationSender.Services.Senders;
using NotificationSender.Services.Senders.Push;



namespace NotificationSender.UnitTests;

public class PushTests
{
    private INotificationSender<AndroidMessage> _androidSender;

    public PushTests()
    {
        var delayMock = new Mock<IDelayProvider>();
        delayMock.Setup(m => m.GetDelay()).Returns(0);
        _androidSender = new AndroidPushSender(delayMock.Object);
    }

    [TestCase(true)]
    [TestCase(true)]
    [TestCase(true)]
    [TestCase(true)]
    [TestCase(false)]
    public void PushSend_FailsAt5thAttempt(bool expected)
    {
        var androidStub = new AndroidMessage();
        var result = _androidSender.SendMessageAsync(androidStub).Result;
        Assert.AreEqual(result, expected);
    }

    [TestCase(SendMessageStates.Created, "created")]
    [TestCase(SendMessageStates.Failed, "failed")]
    [TestCase(SendMessageStates.Successful, "successful")]
    public void SendMessageStates_GetString_ReturnsSuccess(SendMessageStates state, string value)
    {
        var result = state.GetString();
        Assert.AreEqual(result, value);
    }

    public void SendMessageStates_GetString_ThrowsException()
    {
        var state = (SendMessageStates)4;
        Assert.Throws<NotImplementedException>(() => state.GetString());
    }

    [TestCase("created", SendMessageStates.Created)]
    [TestCase("failed", SendMessageStates.Failed)]
    [TestCase("successful", SendMessageStates.Successful)]
    public void SendMessageStates_FromString_ReturnsSuccess(string value, SendMessageStates state)
    {
        var result = value.ToSendMessageState();
        Assert.AreEqual(result, state);
    }

    public void SendMessageStates_FromString_ThrowsException()
    {
        var value = "unknown";
        Assert.Throws<NotImplementedException>(() => value.ToSendMessageState());
    }
}